# Enoncer du TP (en binôme)

## Pré-requis

- Avoir [GIT](https://git-scm.com/downloads) d'installer sur son poste de travail
- Avoir une [JDK8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) d'installer sur son poste de travail (Ajoutez un accès au PATH)
- Avoir [Maven](http://apache.mirrors.nublue.co.uk/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip) d'installer sur son poste (Ajoutez un accès au PATH)

### Test des pré-requis

- Ouvrez un terminal, chaque commande doit retourner la version de l'outil

```
git --version
java -version
mvn -v
 ```

## Astuces

- Gitlab inject des variables disponibles dans la CI, vous trouverez la liste [ici](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- Vous pouvez aussi injecter vos propres variables dans **Settings > CICD**

## Préparation

1. Une personne par groupe doit récupérer le projet dans son espace personnel Gitlab
2. Cette personne doit ajouter son binôme en tant que developer
3. Chaque personne clone le projet sur son poste de travail

## Partie 1 - Configuration du repository

1. Configurez le repository pour que personne ne puisse push directement sur la branche master
2. Configurez le repository pour qu'il y ait au moins 1 approver sur une merge request
3. Pour tester, créez une issue dans gitlab et initier une merge request à partir de l'issue

## Partie 2 - Merge une modification dans la branche master

1. Sur un PC essayez de packager l'application en utilisant Maven
2. Corrigez l'erreur en utilisant la branche précédemment créée
3. Mergez la branche dans master

## Partie 3 - Initialisation du pipeline dans gitlab-ci

1. Créez une nouvelle issue + merge request
2. Faite en sorte que gitlab-ci build votre projet
3. Une fois le build passé créer un autre job permettant de lancer les tests unitaires

## Partie 4 - Code Quality

1. Créez vous un compte sur [SonarCloud](https://sonarcloud.io/) et créez votre Organization
2. Créez une nouvelle issue + merge request
3. Modifiez votre pom.xml permettant l'utilisation de sonar
4. Modifiez votre pipeline pour que gitlab-ci execute sonar

## Partie 5 - Versionning (Création de Tag)

1. Créez une nouvelle issue + merge request
2. Faite en sorte de pouvoir créer un tag via l'interface de gitlab-ci déclanchant un job manuel qui exécute:  
    a. mvn release:prepare  
    b. mvn release:perform  
3. Créez vous un compte sur [myMavenRepo](https://mymavenrepo.com/)
4. L'exécutable créé doit-être uploadé sur votre compte *myMavenRepo*
5. Vérfiez la présence du tag dans gitlab-ci

## Partie 6 - Deploiement

1. Créez une nouvelle issue + merge request
2. Faite en sorte que votre application se deploie automatiquement sur Heroku, une fois le tag créé
